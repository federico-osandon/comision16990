import { useEffect, useState } from "react";
// import { getAlumnos } from "./helpers/getAlumnos";


export default function App() {
  const [alumnos, setAlumnos] = useState([]);

    const getFetch = async ()=>{
        const dataJson = await fetch('assets/DATA.json')
        const respuesta=  await dataJson.json()
        setAlumnos(respuesta.alumnos)
    }

    useEffect(() => {
        // getAlumnos.then((resp) => setAlumnos(resp));
        getFetch()
        
    }, []);

  console.log(alumnos);

    const handlerOnChange = (e) => {
        console.log(e.target.value);
    };

    return (
        <div className="App">
            <select onChange={handlerOnChange}>
                {alumnos.map(alumno => <option key={alumno.id} value={alumno.id}>{ alumno.nombre }</option>)}
            </select>
        </div>
    );
}

