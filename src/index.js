import React from 'react';
import ReactDOM from 'react-dom';
import AppEcommerce from './AppEcommerce';
import './index.css';



ReactDOM.render(  
    <AppEcommerce /> ,
    document.getElementById('root')
);
