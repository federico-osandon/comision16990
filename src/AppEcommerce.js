import {BrowserRouter, Routes, Route} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import NavBar from './components/Navbar/NavBar';
import ItemListContainer from './components/ItemListContainer/ItemListContainer';
import Cart from './components/Cart/Cart';
import ItemDetailContainer from './components/ItemDetail/ItemDetailContainer';
import CartContextProvider from './context/CartContext';



function AppEcommerce() { 
     
    
    return (
        <div  className=' border border-3 border-primary'>
            <center>
               <CartContextProvider >
                    <BrowserRouter >
                        <NavBar />
                        <Routes>
                            <Route path="/" element={<ItemListContainer />} />
                            <Route path="/categoria/:id" element={<ItemListContainer />} />
                            <Route path="/detalle/:id" element={<ItemDetailContainer />} />
                            <Route path="/cart" element={<Cart />} />
                        </Routes>
                    </BrowserRouter>
               </CartContextProvider>               
            </center>
        </div >
        )

}

export default AppEcommerce;

