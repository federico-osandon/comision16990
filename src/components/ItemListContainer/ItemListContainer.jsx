import { getFirestore } from '../../helpers/getFirestore'
import {useParams} from 'react-router-dom'
import {useState, useEffect} from 'react'
import { Link } from 'react-router-dom'
import { getFetch } from '../../helpers/getFetch'


function ItemListContainer() {
    const [products, setProducts] = useState([])  
    const [loading, setLoading] = useState(true)  
    const [bool, setBool] = useState(true) 
    const { categoryID } = useParams()
    
    useEffect(() => {
        const dbQuery = getFirestore() // conexion con firestore

        // dbQuery.collection('items').doc('o0SRMKuaZ1nik6jwLDQ3').get() // traer uno por el id
        // .then(resp => setProd( { id: resp.id, ...resp.data() } ))

        if (categoryID) {
            dbQuery.collection('items').where('categoria', '==', categoryID).get() // traer todo
            .then(data => setProducts(   data.docs.map(pro => ( { id: pro.id, ...pro.data() } ))   ))
            .catch(err=> console.log(err))
            .finally(()=> setLoading(false))    

        } else {                
            dbQuery.collection('items').get() // traer todo
            .then(data => setProducts(   data.docs.map(pro => ( { id: pro.id, ...pro.data() } ))   ))
            .catch(err=> console.log(err))
            .finally(()=> setLoading(false))
        }

    },[])
    
    console.log('antes del rendering')
    console.log(products)
    ////     [<li>1</li>, <li>2</li>,....]

    return (
        
        <div className="App">   
                   
            { loading ? <h1>Cargando...</h1> :  products.map(prod => <div key={prod.id} className="card w-50 mt-5" >
                                        <div className="card-header">
                                            {prod.nombre} {prod.categoria}
                                        </div>
                                        <div className="card-body">
                                            <img src={prod.urlImagen} alt="foto" />
                                            {prod.price}
                                        
                                        </div>
                                        <div className="card-footer">
                                            <Link to={`/detalle/${prod.id}`}>
                                                <button className="btn btn-outline-primary btn-block">
                                                    detalle de persona
                                                </button>
                                            </Link>                                        
                                        </div>
                                        
                                    </div> 
            )} 
         
       
        </div>
    )
}

export default ItemListContainer
