// import {Navbar, Container, Nav, NavDropdown} from 'react-bootstrap'

import Navbar from 'react-bootstrap/Navbar'
import Container from 'react-bootstrap/Container'
import Nav from 'react-bootstrap/Nav'
import NavDropdown from 'react-bootstrap/NavDropdown'
import CartWidget from './CartWidget'
import { Link } from 'react-router-dom'

const NavBar = () => {
    return (       
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Container>
                    <Link to="/">Fede El Mejor Ecommerce</Link>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    <Navbar.Collapse id="responsive-navbar-nav">
                        <Nav className="me-auto">
                        <Nav.Link >
                            <Link to='/categoria/gorras'>
                                Gorras
                            </Link>    
                        </Nav.Link>
                        <Nav.Link href="#pricing">
                        <Link to='/categoria/remeras'>
                                Remeras
                            </Link>
                        </Nav.Link>                       
                       
                        </Nav>
                        <Nav>
                            <Link to="/cart">
                                <CartWidget />
                            </Link>                            
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>       
    )
}

export default NavBar
