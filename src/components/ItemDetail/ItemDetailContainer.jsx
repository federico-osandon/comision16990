import {useState, useEffect} from 'react'
import {useParams} from 'react-router-dom'
import Caso1 from '../../clases/clase9/Caso1'
import Caso2 from '../../clases/clase9/Caso2'
import { Input } from '../../clases/clase9/Input'
import Intercambiabilidad from '../../clases/clase9/Intercambiabilidad'
import { ViewPort } from '../../clases/clase9/ViewPort'
import { getFetchUno } from '../../helpers/getFetch'
import { getFirestore } from '../../helpers/getFirestore'
import ItemDetail from './ItemDetail'

const ItemDetailContainer = () => {
    const [loading, setLoading] = useState(true)
    const [prod, setProd] = useState({})
    const {id} = useParams()

    useEffect(() => {
        const db = getFirestore()
        db.collection('items').doc(id).get()
        .then( res => {        
            console.log('llamada a api') // alguna accion con la respuesta  
            setProd( {id: res.id, ...res.data()} )
        })    
        .catch(err => console.log(err))
        .finally(()=> setLoading(false))
        
        // eslint-disable-next-line       
    },[]) 
 
    
    return (
        <>
             {loading ? 
                    <h2>Cargando...</h2>
                :
                    <div className='border border-3 border-secondary'>
                        <ItemDetail producto={prod} />                        
                    </div>
            }            
        </>
    )
}

export default ItemDetailContainer
