import {useState } from 'react'
import { Col, Row } from 'react-bootstrap'
import { useCartContext } from '../../context/CartContext'
import ItemCount from './ItemCount'


const ItemDetail =  ( {producto} ) => {
    const [count, setCount] = useState(1)

    const { cartList, agregarProducto } = useCartContext()

    function onAdd(cant) {
        setCount(cant)
        agregarProducto( { ...producto, cantidad: cant} )
    }
    console.log(cartList)

    return (
        <Row>
                <label>Soy el detalle</label>
                <Col>                
                    <div className='card w-50'>
                        <div className="container">
                            <label>{producto.nombre}</label>
                        </div>
                        <div className="container">
                            <img  src={producto.urlImagen} className="w-25" alt="foto" />
                            <br/>
                            <label>{producto.descripcion}</label><br/>
                            <label>{producto.categoria}</label>
                        </div>
                        <div className="container">
                            <label>{producto.price}</label>
                        </div>
                    </div>
                </Col>
                <Col>
                    <button onClick={()=>onAdd(3) }>Agregar Carrito</button>
                    <ItemCount onAdd={onAdd} count ={count}/>      
                </Col>                           
            </Row>
    )
}

export default ItemDetail
